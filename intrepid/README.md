# Intrepid

## Screenshots
|    |
|----|
| ![](./2023-06-20_14-1687287204.png) |
| ![](./2023-06-20_14-1687287501.png) |

## Details:
- DE: sway
- Font: [Jeh](https://www.tata.com/newsroom/heritage/jeh-jrd-tata-font)
- Wallpaper: [Ship](https://pixabay.com/illustrations/ship-shipwreck-sea-waves-tall-ship-1366926/)
