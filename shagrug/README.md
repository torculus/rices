# Shagrug

## Screenshots
|    |
|----|
| ![](./2023-07-03_13-1688406961.png) |
| ![](./2023-07-03_13-1688406635.png) |
| ![](./2023-07-03_14-1688407454.png) |

## Details:
- DE: sway
- Font: [Mystery Quest](https://fonts.google.com/specimen/Mystery+Quest?query=mystery)
- Wallpaper: [Pixabay](https://pixabay.com/photos/wallpaper-old-texture-background-3419273/)
